export interface UserModel {
  Body: {
    Name: string;
    FatherLastName: string;
    MotherLastName: string;
    Email: string;
    PhoneNumber: string;
    UserName: string;
    password: string;
    Metadata?: string;
    Tenant?: string;
    Roles: Roles[];
  }
}

interface Roles {
  Id: number,
  Name: string;
}