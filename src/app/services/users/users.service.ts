import { UserModel } from './../../models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchUserModel } from 'src/app/models/search-user.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  endpoint = 'https://techhub.docsolutions.com/OnBoardingPre/WebApi/api/user';
              // https://techhub.docsolutions.com/OnBoardingPre/WebApi/api/user/RegisterUserRole
  constructor(private httpClient: HttpClient) { }

  /**
   * Se realiza la busqueda de usuario en base a lo ingresado por el usuario
   * @param searchData 
   * @returns 
   */
  public getUsers(searchData: SearchUserModel): Observable<any> {
    const header = new HttpHeaders({'Authorization':`Bearer ${localStorage.getItem('token')}`});
    return this.httpClient.post(`${this.endpoint}/GetUsers`, searchData, {headers: header});
  }
 
  /**
   * Se realiza el consumo de servicio para registrar un usuario
   * @param createUserData datos del usuario por guardar
   * @returns 
   */
  public createUser(createUserData: UserModel): Observable<any> {
    const header = new HttpHeaders({'Authorization':`Bearer ${localStorage.getItem('token')}`});
    return this.httpClient.post(`${this.endpoint}/RegisterUserRole`, createUserData, {headers: header});
  }
}
