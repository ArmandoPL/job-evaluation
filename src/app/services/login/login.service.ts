import { LoginModel } from './../../models/login.component';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  endpoint = 'https://techhub.dscloud.com.mx/OBEnrolamientoCap/WebApi/api/authentication/authentication';

  constructor(private httpClient: HttpClient) { }

  /**
   * servicio que consume el endpoint del login
   * @param loginData Objeto del  login del usuario
   * @returns respuesta del login, si exitoso o no
   */
  public login(loginData: LoginModel): Observable<any> {
    return this.httpClient.post(this.endpoint, loginData);
  }
}
