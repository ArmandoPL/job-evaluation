import { UsersService } from './../../services/users/users.service';
import { SearchUserModel } from './../../models/search-user.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  searchForm!: FormGroup;
  searchData: SearchUserModel = {
    Body: {
      SearchText: ''
    }
  }
  users: any[] = [];
  
  
  constructor(private formBuilder: FormBuilder, private userService: UsersService) { }

  ngOnInit(): void {
    this.initSearchForm();
  }

  /**
   * Se inicializa formulario de busqueda
   */
  initSearchForm() {
    this.searchForm = this.formBuilder.group({
      search: ['', Validators.compose([Validators.required])],
    });

    this.searchForm.get('search')?.valueChanges.subscribe((username: string) => {
      this.searchData.Body.SearchText = username;
    });
  }

  searchUsers() {
    this.userService.getUsers(this.searchData).subscribe(data =>{
      this.users = data.Body;      
    });
    
  }

}
