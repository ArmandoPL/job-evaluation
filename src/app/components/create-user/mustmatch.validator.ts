import { FormGroup } from '@angular/forms';
export function MustMatch(controlName: string, matchingControlName: string): any {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const marchingControl = formGroup.controls[matchingControlName];
    if (marchingControl.errors && !marchingControl.errors.nustMatch) {
      return;
    } else {
      marchingControl.setErrors(null);
    }
  }
}