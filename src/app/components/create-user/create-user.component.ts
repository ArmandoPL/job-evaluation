import { UsersService } from './../../services/users/users.service';
import { UserModel } from './../../models/user.model';
import { EmailValidator, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MustMatch } from './mustmatch.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  userForm!: FormGroup;
  isFormOk: boolean = true;
  userModel: UserModel = {
    Body: {
      Name: '',
      FatherLastName: '',
      MotherLastName: '',
      Email: '',
      PhoneNumber: '',
      UserName: '',
      password: '',
      Metadata: undefined,
      Tenant: undefined,
      Roles: [{ Id: 2, Name: "Usuario Tradicional" }]
    }
  };
  message: string = '';


  constructor(
    private formBuilder: FormBuilder, 
    private userService: UsersService,
    private router: Router) { }

  ngOnInit(): void {
    this.initUserForm();
  }

  /**
   * Se inicializa el formulario para crear un nuevo usuario
   */
  initUserForm(): void {
    this.userForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      fatherLastName: ['', Validators.compose([Validators.required])],
      motherLastName: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      user: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      confirmPassword: ['', Validators.compose([Validators.required])],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    this.userForm.get('name')?.valueChanges.subscribe((name: string) => {
      this.userModel.Body.Name = name
    });
    this.userForm.get('fatherLastName')?.valueChanges.subscribe((fatherLastName: string) => {
      this.userModel.Body.FatherLastName = fatherLastName
    });
    this.userForm.get('motherLastName')?.valueChanges.subscribe((motherLastName: string) => {
      this.userModel.Body.MotherLastName = motherLastName
    });
    this.userForm.get('email')?.valueChanges.subscribe((email: string) => {
      this.userModel.Body.Email = email
    });
    this.userForm.get('phone')?.valueChanges.subscribe((phone: string) => {
      this.userModel.Body.PhoneNumber = phone
    });
    this.userForm.get('user')?.valueChanges.subscribe((user: string) => {
      this.userModel.Body.UserName = user
    });
    this.userForm.get('password')?.valueChanges.subscribe((password: string) => {
      this.userModel.Body.password = password
    });
  }

  /**
   * Se realiza la creacion de un usuario
   */
  createUser(): void {
    this.userService.createUser(this.userModel).subscribe(data => {
      if (data.IsOK) {
        this.router.navigate(['users']);
      } else {
        this.isFormOk = false;
        this.message = data.Messages;
      }
      
    });
    
  }
}
