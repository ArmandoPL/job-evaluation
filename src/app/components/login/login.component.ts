import { LoginService } from './../../services/login/login.service';
import { LoginModel } from './../../models/login.component';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  loginData: LoginModel = {
    Body: {
      Username: '',
      Password: ''
    }
  };
  isLoginOk: boolean =true;
  constructor(
    private formBuilder: FormBuilder, 
    private loginService: LoginService,
    private router: Router) { }

  ngOnInit(): void {
    this.initLoginForm();
  }

  /**
   * Se inicializa el formulario del login
   */
  initLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });

    this.loginForm.get('username')?.valueChanges.subscribe((username: string) => {
      this.loginData.Body.Username = username
    });
    this.loginForm.get('password')?.valueChanges.subscribe((password: string) => {
      this.loginData.Body.Password = password
    });
  }

  /**
   * Se realiza el login
   * Se valida si el login fue exitoso y de ser asi redirecciona
   * De no ser asi devuelve un mensaje
   */
  login() {
    this.loginService.login(this.loginData).subscribe((session: any) => {
      if (session.IsOK) {
        localStorage.setItem('token', session.Body.Token);
        this.router.navigate(['users']);
      } else {
        this.isLoginOk = false;
      }
    });
    
  }
}
